package br.senai.sp.informatica.todolist.rest;

/**
 * Created by José Roberto on 12/03/2017.
 */

public class RestAddress {
    // endereço base da API
    private static final String URL = "http://192.168.2.203/ToDoListTarde";
    // endereços dos recursos da API
    public static final String LOGIN = URL + "/login";
    public static final String USUARIO = URL + "/usuario";
    public static final String TAREFAS = URL + "/tarefa";
    public static final String TAREFAS_ABERTAS = URL + "/tarefa/abertas";
    public static final String DELETE_TAREFA = URL + "/tarefa/%d";
    public static final String UPDATE_SUBTAREFA = URL + "/subtarefa/%d";
    public static final String DELETE_SUBTAREFA = URL + "/subtarefa/%d";
    public static final String BUSCAR_TAREFA = URL + "/tarefa/%d";
    public static final String ADD_SUBTAREFA = URL + "/tarefa/%d/subtarefa";
}
