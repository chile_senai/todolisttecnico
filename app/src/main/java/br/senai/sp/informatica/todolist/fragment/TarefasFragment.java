package br.senai.sp.informatica.todolist.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import br.senai.sp.informatica.todolist.R;
import br.senai.sp.informatica.todolist.activity.DetalheTarefaActivity;
import br.senai.sp.informatica.todolist.adapter.TarefaAdapter;
import br.senai.sp.informatica.todolist.modelo.Tarefa;
import br.senai.sp.informatica.todolist.rest.RestAddress;
import br.senai.sp.informatica.todolist.tasks.HandlerTask;
import br.senai.sp.informatica.todolist.tasks.HandlerTaskAdapter;
import br.senai.sp.informatica.todolist.tasks.TaskRest;
import br.senai.sp.informatica.todolist.util.JsonParser;
import br.senai.sp.informatica.todolist.util.PrefsUtil;

/**
 * Created by joserobertochilesilva on 28/01/17.
 */

public class TarefasFragment extends Fragment {

    public enum TipoLista {TODOS, EM_ABERTO}

    TarefaAdapter adapter;
    TipoLista tipoLista;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    SwipeRefreshLayout swipeLayout;
    TaskRest taskListar;
    JsonParser<Tarefa> parserTarefa;
    List<Tarefa> tarefas;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tipoLista = TipoLista.valueOf(getArguments().get("tipoLista").toString());

        // instancia o objeto parserTarefa, que será utilizado mais de uma vez para converter objeto em JSON e vice-versa
        parserTarefa = new JsonParser<>(Tarefa.class);

        // instancia o objeto taskListar, que será usado mais de uma vez para listagem das tarefas

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // infla a view no fragment
        View view = inflater.inflate(R.layout.fragment_tarefas, container, false);

        // recyvlerView
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        // progressBar
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        // swipeLayout
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        swipeLayout.setOnRefreshListener(onRefreshListener);


        // retorna a view
        return view;
    }

    private SwipeRefreshLayout.OnRefreshListener onRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {

            carregarLista();
        }
    };

    private void carregarLista() {
        switch (tipoLista) {
            case EM_ABERTO:
                new TaskRest(TaskRest.RequestMethod.GET, handlerListar, PrefsUtil.getToken(getContext())).execute(RestAddress.TAREFAS_ABERTAS);
                break;
            case TODOS:
                new TaskRest(TaskRest.RequestMethod.GET, handlerListar, PrefsUtil.getToken(getContext())).execute(RestAddress.TAREFAS);
                break;
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        carregarLista();
    }


    private TarefaAdapter.OnTarefaClickListener listenerTarefa = new TarefaAdapter.OnTarefaClickListener() {
        @Override
        public void onClickTarefa(View view, Tarefa tarefa) {
            Intent intent = new Intent(getActivity(), DetalheTarefaActivity.class);
            intent.putExtra("tarefa", tarefa);
            startActivity(intent);
        }
    };

    private TarefaAdapter.OnTarefaClickListener listenerExcluir = new TarefaAdapter.OnTarefaClickListener() {
        @Override
        public void onClickTarefa(View view, Tarefa tarefa) {
            new TaskRest(TaskRest.RequestMethod.DELETE, handlerExcluir, PrefsUtil.getToken(getContext())).execute(String.format(RestAddress.DELETE_TAREFA, tarefa.getId()));
        }
    };


    private HandlerTask handlerExcluir = new HandlerTaskAdapter() {
        @Override
        public void onSuccess(String valueRead) {
            carregarLista();
        }

        @Override
        public void onError(Exception erro) {
            Toast.makeText(getActivity(), erro.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    private HandlerTask handlerListar = new HandlerTaskAdapter() {
        @Override
        public void onPreHandle() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onSuccess(String valueRead) {
            tarefas = parserTarefa.toList(valueRead, Tarefa[].class);
            adapter = new TarefaAdapter(getActivity(), tarefas, listenerTarefa, listenerExcluir);
            recyclerView.setAdapter(adapter);
            progressBar.setVisibility(View.GONE);
            swipeLayout.setRefreshing(false);
        }

        @Override
        public void onError(Exception erro) {
            Toast.makeText(getActivity(), erro.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

}
