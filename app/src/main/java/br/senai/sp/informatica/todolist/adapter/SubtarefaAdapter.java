package br.senai.sp.informatica.todolist.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import br.senai.sp.informatica.todolist.R;
import br.senai.sp.informatica.todolist.modelo.Subtarefa;

/**
 * Created by sn1022208 on 26/01/2017.
 */

public class SubtarefaAdapter extends RecyclerView.Adapter<SubtarefaAdapter.SubtarefaViewHolder>  {
    private final Context context;
    private final List<Subtarefa> subtarefas;
    private final OnSubtarefaClickListener listenerExcluir;
    private final OnCheckChangeListener listenerCheck;

    public SubtarefaAdapter(Context context, List<Subtarefa> subtarefas, OnCheckChangeListener listenerCheck, OnSubtarefaClickListener listenerExcluir) {
        this.context = context;
        this.subtarefas = subtarefas;
        this.listenerCheck = listenerCheck;
        this.listenerExcluir = listenerExcluir;
    }

    @Override
    public SubtarefaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_item, parent, false);
        SubtarefaViewHolder holder = new SubtarefaViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(SubtarefaViewHolder holder, int position) {
        final Subtarefa subtarefa = subtarefas.get(position);
        holder.textDescricao.setText(subtarefa.getDescricao());
        holder.chkFeito.setChecked(subtarefa.isFeito());
        holder.chkFeito.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                listenerCheck.onChange(subtarefa, b);
            }
        });

        holder.btExcluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listenerExcluir.onClickSubtarefa(view, subtarefa);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (subtarefas != null) {
            return subtarefas.size();
        }
        return 0;
    }

    public interface OnSubtarefaClickListener {
        public void onClickSubtarefa(View view, Subtarefa subtarefa);
    }

    public interface OnCheckChangeListener {
        public void onChange(Subtarefa subtarefa, boolean check);
    }

    public static class SubtarefaViewHolder extends RecyclerView.ViewHolder {
        TextView textDescricao;
        Button btExcluir;
        CheckBox chkFeito;
        LinearLayout layoutLista;

        public SubtarefaViewHolder(View view) {
            super(view);
            textDescricao = (TextView) view.findViewById(R.id.textDescricao);
            btExcluir = (Button) view.findViewById(R.id.btExcluir);
            layoutLista = (LinearLayout) view.findViewById(R.id.layoutLista);
            chkFeito = (CheckBox) view.findViewById(R.id.ckFeito);
        }
    }
}
