package br.senai.sp.informatica.todolist.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import br.senai.sp.informatica.todolist.R;
import br.senai.sp.informatica.todolist.modelo.Tarefa;

/**
 * Created by sn1022208 on 26/01/2017.
 */

public class TarefaAdapter extends RecyclerView.Adapter<TarefaAdapter.TarefaViewHolder> {
    private final Context context;
    private final List<Tarefa> tarefas;
    private final OnTarefaClickListener listenerTarefa;
    private final OnTarefaClickListener listenerExcluir;

    public TarefaAdapter(Context context, List<Tarefa> tarefas, OnTarefaClickListener listenerTarefa, OnTarefaClickListener listenerExcluir) {
        this.tarefas = tarefas;
        this.context = context;
        this.listenerTarefa = listenerTarefa;
        this.listenerExcluir = listenerExcluir;
    }

    public interface OnTarefaClickListener {
        public void onClickTarefa(View view, Tarefa tarefa);
    }

    @Override
    public void onBindViewHolder(TarefaViewHolder holder, int position) {
        final Tarefa t = tarefas.get(position);
        holder.textTitulo.setText(t.getTitulo());
        if (t.isFeita()) {
            holder.layoutTarefa.setBackgroundColor(Color.GREEN);
        } else {
            holder.layoutTarefa.setBackgroundColor(Color.WHITE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listenerTarefa.onClickTarefa(view, t);
            }
        });

        holder.btExcluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listenerExcluir.onClickTarefa(view, t);
            }
        });
    }

    @Override
    public TarefaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Infla a view do layout
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_lista, parent, false);
        TarefaViewHolder holder = new TarefaViewHolder(view);
        return holder;
    }

    @Override
    public int getItemCount() {
        if (tarefas != null) {
            return tarefas.size();
        }
        return 0;
    }

    // Subclasse de ViewHolder
    public static class TarefaViewHolder extends RecyclerView.ViewHolder {
        TextView textTitulo;
        Button btExcluir;
        LinearLayout layoutTarefa;

        public TarefaViewHolder(View view) {
            super(view);
            // Cria as views para salvar no ViewHolder
            textTitulo = (TextView) view.findViewById(R.id.textTitulo);
            btExcluir = (Button) view.findViewById(R.id.btExcluir);
            layoutTarefa = (LinearLayout) view.findViewById(R.id.layoutLista);
        }
    }
}
