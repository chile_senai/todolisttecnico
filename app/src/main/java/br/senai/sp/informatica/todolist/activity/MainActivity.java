package br.senai.sp.informatica.todolist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import br.senai.sp.informatica.todolist.R;
import br.senai.sp.informatica.todolist.modelo.TokenJwt;
import br.senai.sp.informatica.todolist.modelo.Usuario;
import br.senai.sp.informatica.todolist.rest.RestAddress;
import br.senai.sp.informatica.todolist.tasks.HandlerTask;
import br.senai.sp.informatica.todolist.tasks.HandlerTaskAdapter;
import br.senai.sp.informatica.todolist.tasks.TaskRest;
import br.senai.sp.informatica.todolist.util.JsonParser;
import br.senai.sp.informatica.todolist.util.PrefsUtil;

/**
 * Created by sn1022208 on 09/02/2017.
 */

public class MainActivity extends AppCompatActivity {
    EditText editEmail, editSenha;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // editEmail
        editEmail = (EditText) findViewById(R.id.edit_email);

        // editSenha
        editSenha = (EditText) findViewById(R.id.edit_password);
    }

    public void btLogarClick(View view) {
        if (editEmail.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, R.string.informe_email, Toast.LENGTH_SHORT).show();
        } else if (editSenha.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, R.string.informe_senha, Toast.LENGTH_SHORT).show();
        } else {
            Usuario usuario = new Usuario();
            usuario.setEmail(editEmail.getText().toString().trim());
            usuario.setSenha(editSenha.getText().toString().trim());
            JsonParser<Usuario> parser = new JsonParser<>(Usuario.class);
            new TaskRest(TaskRest.RequestMethod.POST, handlerTask).execute(RestAddress.LOGIN, parser.fromObject(usuario));
        }
    }

    private HandlerTask handlerTask = new HandlerTaskAdapter() {
        @Override
        public void onError(Exception erro) {
            Toast.makeText(getBaseContext(), erro.getMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(String valueRead) {
            TokenJwt token = new JsonParser<TokenJwt>(TokenJwt.class).toObject(valueRead);
            PrefsUtil.saveToken(getBaseContext(), token.getToken());
            startActivity(new Intent(getBaseContext(),PrincipalActivity.class));
            finish();
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.item_prefs) {
            startActivity(new Intent(this, PreferenceActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void btCriarContaClick(View view) {
        startActivity(new Intent(this, NovaContaActivity.class));
    }
}
