package br.senai.sp.informatica.todolist.modelo;

/**
 * Created by sn1022208 on 09/02/2017.
 */

public class TokenJwt {
    private String token;

    public TokenJwt(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
