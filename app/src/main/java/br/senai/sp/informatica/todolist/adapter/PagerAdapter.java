package br.senai.sp.informatica.todolist.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.senai.sp.informatica.todolist.fragment.TarefasFragment;

/**
 * Created by joserobertochilesilva on 29/01/17.
 */

public class PagerAdapter extends FragmentPagerAdapter {

    //private TarefasFragment fragmentUm, fragmentDois;

    public PagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        Bundle args = new Bundle();
        switch (position) {
            case 0:
                args.putString("tipoLista", TarefasFragment.TipoLista.EM_ABERTO.toString());
                TarefasFragment fragmentZero = new TarefasFragment();
                fragmentZero.setArguments(args);
                return fragmentZero;
            case 1:
                args.putString("tipoLista", TarefasFragment.TipoLista.TODOS.toString());
                TarefasFragment fragmentUm = new TarefasFragment();
                fragmentUm.setArguments(args);
                return fragmentUm;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Em Aberto";
            case 1:
                return "Todas";
            default:
                return null;
        }
    }

}
