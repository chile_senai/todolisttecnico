package br.senai.sp.informatica.todolist.activity;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.senai.sp.informatica.todolist.R;
import br.senai.sp.informatica.todolist.adapter.SubtarefaAdapter;
import br.senai.sp.informatica.todolist.modelo.Subtarefa;
import br.senai.sp.informatica.todolist.modelo.Tarefa;
import br.senai.sp.informatica.todolist.rest.RestAddress;
import br.senai.sp.informatica.todolist.tasks.HandlerTask;
import br.senai.sp.informatica.todolist.tasks.HandlerTaskAdapter;
import br.senai.sp.informatica.todolist.tasks.TaskRest;
import br.senai.sp.informatica.todolist.util.JsonParser;
import br.senai.sp.informatica.todolist.util.PrefsUtil;

/**
 * Created by sn1022208 on 26/01/2017.
 */

public class DetalheTarefaActivity extends AppCompatActivity implements DialogInterface.OnClickListener {

    private Tarefa tarefa;
    TextView textTitulo;
    RecyclerView recyclerView;
    SubtarefaAdapter adapter;
    View promptView;
    AlertDialog dialogSubtarefa;
    EditText editInput;
    TextView textView;
    JsonParser<Subtarefa> parserSubtarefa = new JsonParser<>(Subtarefa.class);
    JsonParser<Tarefa> parserTarefa = new JsonParser<>(Tarefa.class);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_lista);

        tarefa = (Tarefa) getIntent().getExtras().get("tarefa");

        textTitulo = (TextView) findViewById(R.id.textTitulo);
        textTitulo.setText(tarefa.getTitulo());

        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(DetalheTarefaActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new SubtarefaAdapter(this, tarefa.getSubtarefas(), listenerCheck, listenerExcluir);
        recyclerView.setAdapter(adapter);

        promptView = getLayoutInflater().inflate(R.layout.prompts, null);

        editInput = (EditText) promptView.findViewById(R.id.editTextDialogUserInput);
        textView = (TextView) promptView.findViewById(R.id.textView1);
        textView.setText(R.string.informe_descricao);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(promptView);
        builder.setPositiveButton(R.string.ok, this);
        builder.setNegativeButton(R.string.cancel, this);

        dialogSubtarefa = builder.create();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogSubtarefa.show();
            }
        });
    }

    private SubtarefaAdapter.OnSubtarefaClickListener listenerExcluir = new SubtarefaAdapter.OnSubtarefaClickListener() {
        @Override
        public void onClickSubtarefa(View view, Subtarefa subtarefa) {
            new TaskRest(TaskRest.RequestMethod.DELETE, handlerDeleteSub, PrefsUtil.getToken(getBaseContext())).execute(String.format(RestAddress.DELETE_SUBTAREFA, subtarefa.getId()), parserSubtarefa.fromObject(subtarefa));
        }
    };

    private SubtarefaAdapter.OnCheckChangeListener listenerCheck = new SubtarefaAdapter.OnCheckChangeListener() {
        @Override
        public void onChange(Subtarefa subtarefa, boolean check) {
            subtarefa.setFeito(check);

            new TaskRest(TaskRest.RequestMethod.PUT, handlerCheck, PrefsUtil.getToken(getBaseContext())).execute
                    (String.format(RestAddress.UPDATE_SUBTAREFA, subtarefa.getId()), parserSubtarefa.fromObject(subtarefa));
        }
    };


    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case DialogInterface.BUTTON_POSITIVE:
                if (!editInput.getText().toString().trim().isEmpty()) {
                    Subtarefa subtarefa = new Subtarefa();
                    subtarefa.setDescricao(editInput.getEditableText().toString().trim());
                    editInput.setText("");
                    new TaskRest(TaskRest.RequestMethod.POST, handlerAdd, PrefsUtil.getToken(getBaseContext())).execute(String.format(RestAddress.ADD_SUBTAREFA, tarefa.getId()), parserSubtarefa.fromObject(subtarefa));
                } else {
                    Toast.makeText(this, R.string.informe_descricao, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private HandlerTask handlerCheck = new HandlerTaskAdapter() {
        @Override
        public void onError(Exception erro) {
            Toast.makeText(DetalheTarefaActivity.this, erro.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    private HandlerTask handlerDeleteSub = new HandlerTaskAdapter() {
        @Override
        public void onError(Exception erro) {
            Toast.makeText(DetalheTarefaActivity.this, erro.getMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(String valueRead) {
            atualizaTarefa();
        }
    };

    private void atualizaTarefa() {
        new TaskRest(TaskRest.RequestMethod.GET, handlerBuscar, PrefsUtil.getToken(getBaseContext())).execute(String.format(RestAddress.BUSCAR_TAREFA, tarefa.getId()));
    }

    private HandlerTask handlerBuscar = new HandlerTaskAdapter() {

        @Override
        public void onSuccess(String valueRead) {
            tarefa = parserTarefa.toObject(valueRead);
            adapter = new SubtarefaAdapter(DetalheTarefaActivity.this, tarefa.getSubtarefas(), listenerCheck, listenerExcluir);
            recyclerView.setAdapter(adapter);
        }

        @Override
        public void onError(Exception erro) {
            Toast.makeText(DetalheTarefaActivity.this, erro.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    private HandlerTask handlerAdd = new HandlerTaskAdapter() {
        @Override
        public void onSuccess(String valueRead) {
            atualizaTarefa();
        }

        @Override
        public void onError(Exception erro) {
            Toast.makeText(DetalheTarefaActivity.this, erro.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };
}
